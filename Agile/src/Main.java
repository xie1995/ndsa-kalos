
public class Main {

	public static void main(String[] args) {
		GUI gui = new GUI();		
		Database system = new Database();
		Customer customer = new Customer();
		
		system.connectToGui(gui);
		gui.connectToDB(system);
		customer.connectToGui(gui);
		gui.connectToCustomer(customer);
		customer.connectToDB(system); 
	}

}
